import React from "react";

function Table(props) {
  return (
    <table>
      <tbody>
        <tr>
          <th>№</th>
          <th>Валюта</th>
          <th>Курс</th>
        </tr>
        {props.data.map((element, index) => {
          return <tr>
            <td>{element.r030}</td>
            <td>{element.txt}</td>
            <td>{element.rate}</td>
          </tr>;
        })}
      </tbody>
    </table>
  );
}

export default Table