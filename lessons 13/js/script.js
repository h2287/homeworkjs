let url = "https://swapi.dev/api/people";
localStorage.user = JSON.stringify([]);
let arr = [];

const data = fetch(url, { method: "get" });

class Person {
  constructor(name, gender, height, skinton, birth, planet) {
    this.name = name;
    this.gender = gender;
    this.height = height;
    this.skinton = skinton;
    this.birth = birth;
    this.planet = planet;
  }

  show() {
    let card = document.createElement("div");
    card.className = "cards";
    document.querySelector(".human_card").append(card);
    let btn = document.createElement("input");
    btn.setAttribute("type", "button");
    btn.setAttribute("value", "Save");
    btn.setAttribute("id", this.name);

    let p1 = document.createElement("p");
    let p2 = document.createElement("p");
    let p3 = document.createElement("p");
    let p4 = document.createElement("p");
    let p5 = document.createElement("p");
    let p6 = document.createElement("p");

    card.append(p1, p2, p3, p4, p6);
    p1.innerText = `name: ${this.name}`;
    p2.innerText = `gender: ${this.gender}`;
    p3.innerText = `height: ${this.height}`;
    p4.innerText = `skinton: ${this.skinton}`;
    p5.innerText = `birth: ${this.birth}`;
    p6.innerText = `planet: ${this.planet}`;
    p6.append(btn);
  }
}
let processing = data.then((element) => element.json());

processing.then((res) => {
  res.results.forEach(element => {
    let user = new Person(
      element.name,
      element.gender,
      element.height,
      element.skin_color,
      element.birth_year,
      element.homeworld
    );
    user.show();
    arr.push(user);
  });
});

document.querySelector(".human_card").addEventListener("click", (e) => {
  if (e.target.type === 'button') {
      arr.forEach((el)=>{
          if(el.name === e.target.id){
              let a = JSON.parse(localStorage.user);
              a.push(el);
              localStorage.user = JSON.stringify(a);
          }
      })
  }
})


