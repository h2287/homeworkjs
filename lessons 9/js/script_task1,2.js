// Task 1 (Timer with color)

let sec = document.querySelector("span:nth-child(3)");
let min = document.querySelector("span:nth-child(2)");
let hour = document.querySelector("span:nth-child(1)");
let startSec;
let countSec = 0;
let countMin = 0;
let countHour = 0;
const second = () => {
  countSec++;
  if (countSec < 10) {
    sec.innerText = `0${countSec}`;
  } if (countSec >= 10) {
    sec.innerText = countSec;
  }
  if (countSec === 60) {
    countSec = 0;
    sec.innerText = `0${countSec}`;
    countMin++;
    if (countMin < 10) {
      min.innerText = `0${countMin}`;
    }
    if (countMin >= 10) {
      min.innerText = countMin;
    }
  }
  if (countMin === 60) {
    countMin = 0;
    min.innerText = `0${countMin}`;
    countHour++;
    if (countHour < 10) {
      hour.innerText = `0${countHour}`;
    }
    if (countHour >= 10) {
      hour.innerText = countHour;
    }
  }
  if (countHour === 60) {
    countHour = 0;
    hour.innerText = `0${countHour}`;
  }
}

let start = document.querySelector(".stopwatch-control button:nth-child(1)");
let display = document.querySelector(".stopwatch-display");
start.onclick = () => {
  startSec = setInterval(second, 1000);
  display.style.backgroundColor = null;
  display.classList.add("green");
}

let stop = document.querySelector(".stopwatch-control button:nth-child(2)");
stop.onclick = () => {
  clearInterval(startSec);
  display.classList.add("red");
  display.classList.remove("green");
}

let reset = document.querySelector(".stopwatch-control button:nth-child(3)");
reset.onclick = () => {
  clearInterval(startSec);
  countSec = 0;
  countMin = 0;
  countHour = 0;
  sec.innerText = `0${countSec}`;
  min.innerText = `0${countMin}`;
  hour.innerText = `0${countHour}`;
  display.classList.add("silver");
  display.classList.remove("red");
  display.classList.remove("green");
}

// Task 2 (Check phone)

let input = document.createElement("input");
document.body.append(input);
let btn = document.createElement("input");
btn.setAttribute("type", "button");
btn.setAttribute("value", "Save");
document.body.append(btn);
let div = document.createElement("div");
input.before(div);
let pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;

let number;
btn.onclick = () => {
  number = input.value;
  if (pattern.test(number)) {
    input.style.backgroundColor = "green";
    document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg';
  } else {
    div.innerText = "Введіть номер телефону у правильному форматі: 097-475-84-58";
  }
}