/* Bиды начинок и добавок */
const [
  SIZE_SMALL,
  SIZE_LARGE,
  STUFFING_CHEESE,
  STUFFING_SALAD,
  STUFFING_POTATO,
  TOPPING_MAYO,
  TOPPING_SPICE,
] = [
    { id: "small", hr: 50, kall: 20 },
    { id: "large", hr: 100, kall: 40 },
    { id: "cheese", hr: 10, kall: 20 },
    { id: "salad", hr: 20, kall: 5 },
    { id: "potato", hr: 15, kall: 10 },
    { id: "mayo", hr: 20, kall: 5 },
    { id: "spice", hr: 15, kall: 0 },
  ];

class Hamburger {
  constructor(size, shuffing) {
    this.size = size;
    this.shuffing = shuffing;
    this.topping = [];
  }
  get size() {
    return this._size;
  }
  set size(size) {
    if (size === SIZE_LARGE || size === SIZE_SMALL) {
      this._size = size;
    } else {
      console.log(
        new Error(
          "Помилка при створенні. Введено невірні данні. Введіть SIZE_LARGE або SIZE_SMALL."
        )
      );
    }
    try {
      if (!size) {
        throw new Error(
          "Введенно неіснуючі данні. Введіть SIZE_LARGE або SIZE_SMALL."
        );
      }
    } catch (error) {
      console.log(error);
    }
  }
  get shuffing() {
    return this._size;
  }
  set shuffing(shuffing) {
    if (
      shuffing === STUFFING_CHEESE ||
      shuffing === STUFFING_POTATO ||
      shuffing === STUFFING_SALAD
    ) {
      this._shuffing = shuffing;
    } else {
      console.log(
        new Error(
          "Помилка при створенні. Введено невірні данні. Введіть STUFFING_CHEESE або STUFFING_POTATO або STUFFING_SALAD."
        )
      );
    }
    try {
      if (!shuffing) {
        throw new Error(
          "Введенно неіснуючі данні. Введіть STUFFING_CHEESE або STUFFING_POTATO або STUFFING_SALAD."
        );
      }
    } catch (error) {
      console.log(error);
    }
  }
  getSize() {
    return this._size.id;
  }
  getShuffing() {
    return this._shuffing.id;
  }

  //Створення функції для додавання топінгу.
  addTopping(topping) {
    //Забороняємо повторення.
    for (let top of Object.values(this.topping)) {
      if (top.id === topping.id) {
        console.log("Цей топпінг уже був доданний. Спробуйте додати інший.");
        return;
      }
    }
    if (topping === TOPPING_MAYO || topping === TOPPING_SPICE)
      this.topping.push(topping);
    else {
      console.log(
        new Error(
          "Топпінг відсутній. Невірні данні. Введіть TOPPING_MAYO або TOPPING_SPICE."
        )
      );
    }
  }

  //Реалізуємо отриманн топпінгу. 
  getTopping() {
    return this.topping;
  }

  //Реалізовуємо функцію видалення топпінгу. 
  removeTopping(topping) {
    if (topping === TOPPING_MAYO || topping === TOPPING_SPICE) {
      this.topping = this.topping.filter((el) => el.id != topping.id);
    } else console.log(
      new Error(
        "Топпінг відсутній. Невірні данні. Введіть TOPPING_MAYO або TOPPING_SPICE."
      )
    );
  }

  //Створюємо функцію для підрахунку ціни.
  calccalculatePrice() {
    let sumMain = 0;
    let sumTopping = 0;
    for (let item of Object.values(this)) {
      if (item.hr) {
        sumTopping += item.hr;
        console.log(sumTopping);
      }
    }
    return sumMain + sumTopping;
  }

  //Створюємо підрахунок каллорій.
  calculateCalories() {
    let sumMain = 0;
    let sumTopping = 0;
    for (let item of Object.values(this)) {
      if (item.kall) {
        sumMain += item.kall;
        console.log(sumMain);
      }
    }
    for (let item of Object.values(this.topping)) {
      if (item.kall) {
        sumTopping += item.kall;
        console.log(sumTopping);
      }
    }
    return sumMain + sumTopping;
  }
}

//Приклад використання. 
const hamburger = new Hamburger(SIZE_LARGE, STUFFING_CHEESE);
console.log(hamburger);

console.log(hamburger.getSize());
console.log(hamburger.getShuffing());

hamburger.addTopping(TOPPING_SPICE);
hamburger.addTopping(TOPPING_SPICE);
hamburger.addTopping(TOPPING_MAYO);
hamburger.addTopping(TOPPING_MAYO);
console.log(hamburger);

console.log(hamburger.getTopping());
hamburger.removeTopping(TOPPING_MAYO);
hamburger.removeTopping(TOPPING_SPICE);
console.log(hamburger);

hamburger.addTopping(TOPPING_MAYO);
console.log(hamburger);

console.log(`Вартість гамбургера становить: ${hamburger.calccalculatePrice()} грн.`);
console.log(`Кількість калорій становить: ${hamburger.calculateCalories()} грн.`);

hamburger.addTopping(TOPPING_SPICE);
console.log(`Вартість гамбургера становить: ${hamburger.calccalculatePrice()} грн.`);

hamburger.removeTopping(TOPPING_MAYO);
console.log(`Кількість калорій становить: ${hamburger.calculateCalories()} грн.`);