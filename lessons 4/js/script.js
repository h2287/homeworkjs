//First function
function map(fn, Array) {
  for(let i = 0; i < Array.length; i++) {
    Array[i] = fn(Array[i]);
  }
  return Array;
};

document.write("First function: " + map(function(a){return a + 1},[1,2,3,4,5]) + "<br>" +"<hr>");

//Second function
function checkAge(age) {
  return age > 18 ? true : confirm("Батьки дозволили?")
};

let x = parseInt(prompt("Your age:"));

document.write("Second function: " + checkAge(x));
