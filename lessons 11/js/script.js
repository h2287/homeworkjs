// 1. Name Surname
const firstName = document.querySelector("#name"),
  surName = document.querySelector("#surname"),
  show = document.querySelector("#show-user"),
  output = document.querySelector(".output");

function validateInput(e) {
  let input = e.target;
  if (/^[a-zа-я]+$/i.test(input.value)) {
    input.style.backgroundColor = "green";
  } else {
    input.style.backgroundColor = "red";
    return false;
  }
}

firstName.addEventListener("change", validateInput);

surName.addEventListener("change", validateInput);

let userArray = [];
let count = 0;
class User {
  createData() {
    console.log(firstName.value + " " + surName.value);
    this.name = firstName.value;
    this.surname = surName.value;
  }
  createUser() {
    let user = new User();
    userArray.push(user);
    count++;
  }
  showUser() {
    output.innerHTML += `${count}. ${this.name} ${this.surname} <br>`;
  }
}

let user = new User();
show.addEventListener("click", () => {
  if (firstName.style.backgroundColor == "green" && surName.style.backgroundColor == "green") {
    user.createData();
    user.createUser();
    user.showUser();
    firstName.value = "";
    surName.value = "";
    firstName.style.backgroundColor = "";
    surName.style.backgroundColor = "";
  } else alert("Input correct data");
})

// 2. List
let li = document.querySelector("ul>li:nth-child(2)");
li.querySelector("ul>li:nth-child(1)").style.backgroundColor = "blue";
li.querySelector("ul>li:nth-child(3)").style.backgroundColor = "red";

let ul = document.querySelector("#ul-color");
let changeColor = document.querySelector("#change-color");
ul.addEventListener("click", (e) => {
  let el = e.target;
  el.style.backgroundColor = changeColor.value;
});

// 3. Field
const task3 = document.querySelector("div.task:nth-child(3)");

let field = document.createElement("div");
field.style.height = "400px";
field.style.width = "100%";
field.style.backgroundColor = "pink"
task3.append(field);

field.addEventListener("mousemove", (e) => {
  field.innerHTML = `Абсолютні координати: Х-${e.pageX} Y-${e.pageY}<br>
  Координати відносно вікна: Х-${e.clientX} Y-${e.clientY}<br>
  Координати відносно баткiвського елемента: Х-${e.offsetX} Y-${e.offsetY}<br>`
});

// 4. Button
function createButton(value) {
  let btn = document.createElement("input");
  btn.type = "button";
  btn.value = `${value}`;
  btnBox.prepend(btn);
}

const btnBox = document.querySelector(".button-box");
btnBox.addEventListener("click", (e) => {
  let btn = e.target;
  if (btn.tagName != "INPUT") return;
  alert(`Натиснута кнопка ${btn.value}`);
})

// 5. Div move
let div = null;
function createDiv(width, height, color, borderRadius) {
  div = document.createElement("div");
  div.style.cssText = `
  width: ${width}px;
  height: ${height}px;
  background-color: ${color};
  border-radius: ${borderRadius}px;
  transition: all 0.2s
  `;
  document.querySelector("div.task:nth-child(5)").append(div);
}

createDiv(50, 50, "red", 40);

div.addEventListener("mouseover", (e) => {
  div.style.position = "absolute";
  let left = div.offsetLeft;
  let top = div.offsetTop;
  let newLeft = left;
  let newTop = top;
  newLeft = e.pageX + div.clientWidth;
  if (newLeft > document.body.clientWidth / 2) {
    newLeft = e.pageX - div.clientWidth * 2;
  }
  newTop = e.pageY - div.clientHeight;
  div.style.left = newLeft + "px";
  div.style.top = newTop + "px";
});

// 6. Color field
const btnColor = document.querySelector("#btn-color");
const bodyColor = document.querySelector("#body-color");
btnColor.addEventListener("click", () => {
  document.body.style.backgroundColor = bodyColor.value;
})

// 7. Dublicate in console
const Login = document.querySelector("#login");
const clearLogin = document.querySelector("#clear");

Login.addEventListener("keypress", (e) => {
  console.log(e.key);
})
clear.addEventListener("click", () =>
  Login.value = ""
)

// 8. Input field
let textField = document.createElement("input");
document.querySelector("div.task:nth-child(8)").append(textField);
textField.before("Введіть текст:");
let info = document.createElement("div");
textField.after(info);
info.style.marginLeft = "7%";
textField.addEventListener("keypress", (e) => {
  info.innerText += e.key;
})