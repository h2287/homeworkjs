import React from 'react';
import ReactDOM from 'react-dom';

const App = function () {

  return (
    <div>
      <Months></Months>
      <Days></Days>
      <Zodiac></Zodiac>
    </div>
  )

}

const Months = function () {
  const moun = ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'];
  const arrMoun = moun.map(element => <li>{element}</li>);

  return (
    <div>
      <h2>Місяці</h2>
      <ul>
        {arrMoun}
      </ul>
    </div>
  )

}

const Days = function () {
  const days = ['Понеділк', 'Вівторок', 'Середа', 'Четвер', 'П`ятниця', 'Субота', 'Неділя'];
  const arrDays = days.map(element => <li>{element}</li>);

  return (
    <div>
      <h2>Дні тиждня</h2>
      <ul>
        {arrDays}
      </ul>
    </div>
  )
}

const Zodiac = function () {
  const zodiac = ['Овен', 'Телець', 'Близнюки', 'Рак', 'Лев', 'Діва', 'Терези', 'Скорпіон', 'Стрілець', 'Козеріг', 'Водолій', 'Риби'];
  const arrZodiac = zodiac.map(element => <li>{element}</li>);
  
  return (
    <div>
      <h2>Знаки Зодіака</h2>
      <ul>
        {arrZodiac}
      </ul>
    </div>
  )
}

ReactDOM.render(<App></App>, document.querySelector("#root"))