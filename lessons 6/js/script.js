//Classwork

/*Створіть функцію-конструктор Calculator, який створює об'єкти з трьома методами:
   1. read () запитує два значення за допомогою prompt і зберігає їх значення у властивостях об'єкта.
  2. sum() возвращает сумму введённых свойств.
  3. mul() возвращает произведение введённых свойств.
*/

function Calculator() {

  this.read = function() {
    this.a = +prompt(`Enter first value`);
    this.b = +prompt(`Enter second value`);
  };

  Calculator.prototype.sum = function() {
    return this.a + this.b;
  };

  Calculator.prototype.mul = function() {
    return this.a * this.b;
  };
}

let calculator = new Calculator();
calculator.read();

document.write(`sum = ${calculator.sum()}; </br>`);
document.write(`mul = ${calculator.mul()}; </br>`);


//Homework

/*  Розробіть функцію-конструктор, яка буде створювати об'єкт Human(людина) створіть масив об'єктів і реалізуйте функцію, яка буде сортувати елементи масиву за значенням властивості Age за зростанням або за спаданням.
*/

function Human(name, age) {
  this.name =name;
  this.age = age;
}

Human.prototype.eldest = function(mensArr) {
  let maxAge = 0;
  mensArr.forEach(function(human) {
    if (human.age > maxAge)
      maxAge = human.age;
  });
  document.write(`Найбільший вік - ${maxAge}; </br>`);
};

let human = new Human();
let mensArr = [];
mensArr.push(new Human("Gleb", 34));
mensArr.push(new Human("Jenny", 57));
mensArr.push(new Human("Ann", 24));
mensArr.push(new Human("Denny", 17));

human.eldest(mensArr);

/*Розробіть функцію-конструктор, яка буде створювати об'єкт Human, додайте на свій розсуд властивості і методи в цей об'єкт.
Подумайте, які методи і властивості слід зробити рівня екземпляра, а які рівня функції-конструктора.
 */

function HumanCustom(name, surname, age, job, hometown) {
  this.name = name;
  this.surname = surname;
  this.age = age;
  this.job = job;
  this.hometown = hometown;
};

let human__developer_Ann = new HumanCustom(`Ann`, `Elfi`, `24`, `Web-developer`, `Odense`);
let human__developer_Jenny = new HumanCustom(`Jenny`, `Viera`, `57`, `QA-developer`, `Aarhus`);
let human__developer_Denny = new HumanCustom(`Denny`, `Rio`, `17`, `JS-developer`, `Grecia`);

HumanCustom.prototype.show = function() {
  let info_developer_Ann = Object.values(human__developer_Ann);
  let info_developer_Jenny = Object.values(human__developer_Jenny);
  let info_developer_Denny = Object.values(human__developer_Denny);

  document.write(`${info_developer_Ann.join(`, `)}; </br>`);
  document.write(`${info_developer_Jenny.join(`, `)}; </br>`);
  document.write(`${info_developer_Denny.join(`, `)}; </br>`);
};

let mens = new HumanCustom();
mens.show();